package ginparamsmapper

import (
	"strconv"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	contextfactory "gitlab.com/Zandraz/gin-context-factory"
)

// customTestType must be use only for the last test case (the default one).
type customTestType struct {
	A string
}

type getFromContextTestData struct {
	ParamLocation ParamLocation
	ParamName     string
	Context       contextfactory.ContextOptions
	Dest          any
	ShouldFail    bool
	ExpectedDest  any
}

var (
	expectedString         = "test_value"
	expectedFloat64        = 1.1
	expectedInt            = 1
	expectedInt64    int64 = 1
	expectedBool           = true
	expectedBytes          = []byte("test")
	expectedUUID           = uuid.MustParse("00000000-0000-0000-0000-000000000001")
	expectedDuration       = time.Second
	expectedTime           = time.Now()
)

func init() {
	var err error
	s := expectedTime.Format(time.RFC3339Nano)
	expectedTime, err = time.Parse(time.RFC3339Nano, s)
	if err != nil {
		panic("Cant parse expected Time. Error : " + err.Error())
	}
}

var getFromContextTestCases = []getFromContextTestData{
	{ // fail missing query param
		ParamLocation: QueryLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			QueryParams: map[string]string{
				"wrong_key": "test_value",
			},
		},
		Dest:       new(string),
		ShouldFail: true,
	},
	{ // success string
		ParamLocation: QueryLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			QueryParams: map[string]string{
				"test": "test_value",
			},
		},
		Dest:         new(string),
		ShouldFail:   false,
		ExpectedDest: &expectedString,
	},
	{ // succes float64
		ParamLocation: QueryLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			QueryParams: map[string]string{
				"test": "1.1",
			},
		},
		Dest:         new(float64),
		ShouldFail:   false,
		ExpectedDest: &expectedFloat64,
	},
	{ // fail invalid float64
		ParamLocation: QueryLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			QueryParams: map[string]string{
				"test": "test",
			},
		},
		Dest:       new(float64),
		ShouldFail: true,
	},
	{ // success int
		ParamLocation: QueryLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			QueryParams: map[string]string{
				"test": "1",
			},
		},
		Dest:         new(int),
		ShouldFail:   false,
		ExpectedDest: &expectedInt,
	},
	{ // fail invalid int
		ParamLocation: QueryLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			QueryParams: map[string]string{
				"test": "test",
			},
		},
		Dest:       new(int),
		ShouldFail: true,
	},
	{ // success int64
		ParamLocation: QueryLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			QueryParams: map[string]string{
				"test": "1",
			},
		},
		Dest:         new(int64),
		ShouldFail:   false,
		ExpectedDest: &expectedInt64,
	},
	{ // fail invalid int64
		ParamLocation: QueryLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			QueryParams: map[string]string{
				"test": "test",
			},
		},
		Dest:       new(int64),
		ShouldFail: true,
	},
	{ // success bool
		ParamLocation: QueryLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			QueryParams: map[string]string{
				"test": "true",
			},
		},
		Dest:         new(bool),
		ShouldFail:   false,
		ExpectedDest: &expectedBool,
	},
	{ // fail invalid bool
		ParamLocation: QueryLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			QueryParams: map[string]string{
				"test": "test",
			},
		},
		Dest:       new(bool),
		ShouldFail: true,
	},
	{ // success []byte
		ParamLocation: QueryLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			QueryParams: map[string]string{
				"test": "test",
			},
		},
		Dest:         &[]byte{},
		ShouldFail:   false,
		ExpectedDest: &expectedBytes,
	},
	{ // success uuid.UUID
		ParamLocation: QueryLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			QueryParams: map[string]string{
				"test": "00000000-0000-0000-0000-000000000001",
			},
		},
		Dest:         &uuid.UUID{},
		ShouldFail:   false,
		ExpectedDest: &expectedUUID,
	},
	{ // fail invalid uuid.UUID
		ParamLocation: QueryLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			QueryParams: map[string]string{
				"test": "test",
			},
		},
		Dest:       &uuid.UUID{},
		ShouldFail: true,
	},
	{ // success time.Time
		ParamLocation: QueryLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			QueryParams: map[string]string{
				"test": expectedTime.Format(time.RFC3339Nano),
			},
		},
		Dest:         &time.Time{},
		ShouldFail:   false,
		ExpectedDest: &expectedTime,
	},
	{ // fail invalid time.Time
		ParamLocation: QueryLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			QueryParams: map[string]string{
				"test": "test",
			},
		},
		Dest:       &time.Time{},
		ShouldFail: true,
	},
	{ // success time.Duration
		ParamLocation: QueryLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			QueryParams: map[string]string{
				"test": "1s",
			},
		},
		Dest:         new(time.Duration),
		ShouldFail:   false,
		ExpectedDest: &expectedDuration,
	},
	{ // fail invalid time.Duration
		ParamLocation: QueryLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			QueryParams: map[string]string{
				"test": "test",
			},
		},
		Dest:       "time.Duration",
		ShouldFail: true,
	},
	{ // fail unsupported type
		ParamLocation: QueryLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			QueryParams: map[string]string{
				"test": "test",
			},
		},
		Dest:       &customTestType{},
		ShouldFail: true,
	},
	{ // fail missing path param
		ParamLocation: PathLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			PathParams: gin.Params{
				gin.Param{
					Key:   "wrong_key",
					Value: "test_value",
				},
			},
		},
		Dest:       new(string),
		ShouldFail: true,
	},
	{ // success string
		ParamLocation: PathLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			PathParams: gin.Params{
				gin.Param{
					Key:   "test",
					Value: "test_value",
				},
			},
		},
		Dest:         new(string),
		ShouldFail:   false,
		ExpectedDest: &expectedString,
	},
	{ // succes float64
		ParamLocation: PathLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			PathParams: gin.Params{
				gin.Param{
					Key:   "test",
					Value: "1.1",
				},
			},
		},
		Dest:         new(float64),
		ShouldFail:   false,
		ExpectedDest: &expectedFloat64,
	},
	{ // fail invalid float64
		ParamLocation: PathLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			PathParams: gin.Params{
				gin.Param{
					Key:   "test",
					Value: "test",
				},
			},
		},
		Dest:       new(float64),
		ShouldFail: true,
	},
	{ // success int
		ParamLocation: PathLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			PathParams: gin.Params{
				gin.Param{
					Key:   "test",
					Value: "1",
				},
			},
		},
		Dest:         new(int),
		ShouldFail:   false,
		ExpectedDest: &expectedInt,
	},
	{ // fail invalid int
		ParamLocation: PathLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			PathParams: gin.Params{
				gin.Param{
					Key:   "test",
					Value: "test",
				},
			},
		},
		Dest:       new(int),
		ShouldFail: true,
	},
	{ // success int64
		ParamLocation: PathLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			PathParams: gin.Params{
				gin.Param{
					Key:   "test",
					Value: "1",
				},
			},
		},
		Dest:         new(int64),
		ShouldFail:   false,
		ExpectedDest: &expectedInt64,
	},
	{ // fail invalid int64
		ParamLocation: PathLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			PathParams: gin.Params{
				gin.Param{
					Key:   "test",
					Value: "test",
				},
			},
		},
		Dest:       new(int64),
		ShouldFail: true,
	},
	{ // success bool
		ParamLocation: PathLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			PathParams: gin.Params{
				gin.Param{
					Key:   "test",
					Value: "true",
				},
			},
		},
		Dest:         new(bool),
		ShouldFail:   false,
		ExpectedDest: &expectedBool,
	},
	{ // fail invalid bool
		ParamLocation: PathLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			PathParams: gin.Params{
				gin.Param{
					Key:   "test",
					Value: "test",
				},
			},
		},
		Dest:       new(bool),
		ShouldFail: true,
	},
	{ // success []byte
		ParamLocation: PathLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			PathParams: gin.Params{
				gin.Param{
					Key:   "test",
					Value: "test",
				},
			},
		},
		Dest:         &[]byte{},
		ShouldFail:   false,
		ExpectedDest: &expectedBytes,
	},
	{ // success uuid.UUID
		ParamLocation: PathLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			PathParams: gin.Params{
				gin.Param{
					Key:   "test",
					Value: "00000000-0000-0000-0000-000000000001",
				},
			},
		},
		Dest:         &uuid.UUID{},
		ShouldFail:   false,
		ExpectedDest: &expectedUUID,
	},
	{ // fail invalid uuid.UUID
		ParamLocation: PathLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			PathParams: gin.Params{
				gin.Param{
					Key:   "test",
					Value: "test",
				},
			},
		},
		Dest:       &uuid.UUID{},
		ShouldFail: true,
	},
	{ // success time.Time
		ParamLocation: PathLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			PathParams: gin.Params{
				gin.Param{
					Key:   "test",
					Value: expectedTime.Format(time.RFC3339Nano),
				},
			},
		},
		Dest:         &time.Time{},
		ShouldFail:   false,
		ExpectedDest: &expectedTime,
	},
	{ // fail invalid time.Time
		ParamLocation: PathLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			PathParams: gin.Params{
				gin.Param{
					Key:   "test",
					Value: "test",
				},
			},
		},
		Dest:       &time.Time{},
		ShouldFail: true,
	},
	{ // success time.Duration
		ParamLocation: PathLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			PathParams: gin.Params{
				gin.Param{
					Key:   "test",
					Value: "1s",
				},
			},
		},
		Dest:         new(time.Duration),
		ShouldFail:   false,
		ExpectedDest: &expectedDuration,
	},
	{ // fail invalid time.Duration
		ParamLocation: PathLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			PathParams: gin.Params{
				gin.Param{
					Key:   "test",
					Value: "test",
				},
			},
		},
		Dest:       new(time.Duration),
		ShouldFail: true,
	},
	{ // fail unsupported type
		ParamLocation: PathLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			PathParams: gin.Params{
				gin.Param{
					Key:   "test",
					Value: "test",
				},
			},
		},
		Dest:       &customTestType{},
		ShouldFail: true,
	},
	{ // fail missing var
		ParamLocation: ContextVarLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			ContextVar: map[string]any{
				"wrong_key": "test_value",
			},
		},
		Dest:       new(string),
		ShouldFail: true,
	},
	{ // success string
		ParamLocation: ContextVarLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			ContextVar: map[string]any{
				"test": "test_value",
			},
		},
		Dest:         new(string),
		ShouldFail:   false,
		ExpectedDest: &expectedString,
	},
	{ // succes float64
		ParamLocation: ContextVarLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			ContextVar: map[string]any{
				"test": 1.1,
			},
		},
		Dest:         new(float64),
		ShouldFail:   false,
		ExpectedDest: &expectedFloat64,
	},
	{ // fail invalid float64
		ParamLocation: ContextVarLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			ContextVar: map[string]any{
				"test": "test",
			},
		},
		Dest:       new(float64),
		ShouldFail: true,
	},
	{ // success int
		ParamLocation: ContextVarLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			ContextVar: map[string]any{
				"test": 1,
			},
		},
		Dest:         new(int),
		ShouldFail:   false,
		ExpectedDest: &expectedInt,
	},
	{ // fail invalid int
		ParamLocation: ContextVarLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			ContextVar: map[string]any{
				"test": "test",
			},
		},
		Dest:       new(int),
		ShouldFail: true,
	},
	{ // success int64
		ParamLocation: ContextVarLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			ContextVar: map[string]any{
				"test": int64(1),
			},
		},
		Dest:         new(int64),
		ShouldFail:   false,
		ExpectedDest: &expectedInt64,
	},
	{ // fail invalid int64
		ParamLocation: ContextVarLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			ContextVar: map[string]any{
				"test": "test",
			},
		},
		Dest:       new(int64),
		ShouldFail: true,
	},
	{ // success bool
		ParamLocation: ContextVarLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			ContextVar: map[string]any{
				"test": true,
			},
		},
		Dest:         new(bool),
		ShouldFail:   false,
		ExpectedDest: &expectedBool,
	},
	{ // fail invalid bool
		ParamLocation: ContextVarLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			ContextVar: map[string]any{
				"test": "test",
			},
		},
		Dest:       new(bool),
		ShouldFail: true,
	},
	{ // success []byte
		ParamLocation: ContextVarLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			ContextVar: map[string]any{
				"test": []byte("test"),
			},
		},
		Dest:         &[]byte{},
		ShouldFail:   false,
		ExpectedDest: &expectedBytes,
	},
	{ // success uuid.UUID
		ParamLocation: ContextVarLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			ContextVar: map[string]any{
				"test": uuid.MustParse("00000000-0000-0000-0000-000000000001"),
			},
		},
		Dest:         &uuid.UUID{},
		ShouldFail:   false,
		ExpectedDest: &expectedUUID,
	},
	{ // fail invalid uuid.UUID
		ParamLocation: ContextVarLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			ContextVar: map[string]any{
				"test": "test",
			},
		},
		Dest:       &uuid.UUID{},
		ShouldFail: true,
	},
	{ // success time.Time
		ParamLocation: ContextVarLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			ContextVar: map[string]any{
				"test": expectedTime,
			},
		},
		Dest:         &time.Time{},
		ShouldFail:   false,
		ExpectedDest: &expectedTime,
	},
	{ // fail invalid time.Time
		ParamLocation: ContextVarLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			ContextVar: map[string]any{
				"test": "test",
			},
		},
		Dest:       &time.Time{},
		ShouldFail: true,
	},
	{ // success time.Duration
		ParamLocation: ContextVarLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			ContextVar: map[string]any{
				"test": time.Second,
			},
		},
		Dest:         new(time.Duration),
		ShouldFail:   false,
		ExpectedDest: &expectedDuration,
	},
	{ // fail invalid time.Duration
		ParamLocation: ContextVarLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			ContextVar: map[string]any{
				"test": "test",
			},
		},
		Dest:       new(time.Duration),
		ShouldFail: true,
	},
	{ // fail unsupported type
		ParamLocation: ContextVarLocation,
		ParamName:     "test",
		Context: contextfactory.ContextOptions{
			Path: "/",
			ContextVar: map[string]any{
				"test": "test",
			},
		},
		Dest:       &customTestType{},
		ShouldFail: true,
	},
}

func TestGetFromContext(t *testing.T) {
	for i, testCase := range getFromContextTestCases {
		t.Run("Case "+strconv.Itoa(i), func(t *testing.T) {
			c, _ := contextfactory.BuildGinTestContext(testCase.Context)
			err := GetFromContext(testCase.ParamLocation, testCase.ParamName, c, testCase.Dest)

			if testCase.ShouldFail {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
				assert.Equal(t, testCase.ExpectedDest, testCase.Dest)
			}
		})
	}
}
