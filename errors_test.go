package ginparamsmapper

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMissingQueryParamError(t *testing.T) {
	e := MissingParamError{
		Param:    "test",
		Location: "query",
	}
	assert.Equal(t, "missing query param test in the context", e.Error())
}

func TestNotCastableQueryParamError(t *testing.T) {
	e := NotCastableParamError{
		Param: "test",
		Type:  "test",
		Value: "test",
		Err:   errors.New("test"),
	}
	assert.Equal(t, "test (test) not castable into test. Error : test", e.Error())
}
